package com.pkbigdata.entity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * 用户登录端口数据记录
 * Created by Administrator on 2016/1/20.
 */
@Entity
@Table(name = "dc_port_info", catalog = "dc")
public class DcPortInfo implements java.io.Serializable{
    private int id;
    private String userName;//登录者姓名
    private Integer userId;//登录者id
    private String ip;//登录地IP
    private Timestamp loginTime;//登录时间
    private String browser;//使用的浏览器
    private String area;//用户当前地址
    private String portType;//判断用户是pc，还是移动
    private Timestamp loginOutTime;
    private Double onlineTime;

    @Id
    @GeneratedValue
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "user_name")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Basic
    @Column(name = "user_id")
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "ip")
    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @Basic
    @Column(name = "login_time")
    public Timestamp getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Timestamp loginTime) {
        this.loginTime = loginTime;
    }

    @Basic
    @Column(name = "browser")
    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    @Basic
    @Column(name = "area")
    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    @Basic
    @Column(name = "port_type")
    public String getPortType() {
        return portType;
    }

    public void setPortType(String portType) {
        this.portType = portType;
    }

    @Basic
    @Column(name = "login_out_time")
    public Timestamp getLoginOutTime() {
        return loginOutTime;
    }

    public void setLoginOutTime(Timestamp loginOutTime) {
        this.loginOutTime = loginOutTime;
    }

    @Basic
    @Column(name = "online_time")
    public Double getOnlineTime() {
        return onlineTime;
    }

    public void setOnlineTime(Double onlineTime) {
        this.onlineTime = onlineTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DcPortInfo that = (DcPortInfo) o;

        if (id != that.id) return false;
        if (userName != null ? !userName.equals(that.userName) : that.userName != null) return false;
        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;
        if (ip != null ? !ip.equals(that.ip) : that.ip != null) return false;
        if (loginTime != null ? !loginTime.equals(that.loginTime) : that.loginTime != null) return false;
        if (browser != null ? !browser.equals(that.browser) : that.browser != null) return false;
        if (area != null ? !area.equals(that.area) : that.area != null) return false;
        if (portType != null ? !portType.equals(that.portType) : that.portType != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (userName != null ? userName.hashCode() : 0);
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        result = 31 * result + (ip != null ? ip.hashCode() : 0);
        result = 31 * result + (loginTime != null ? loginTime.hashCode() : 0);
        result = 31 * result + (browser != null ? browser.hashCode() : 0);
        result = 31 * result + (area != null ? area.hashCode() : 0);
        result = 31 * result + (portType != null ? portType.hashCode() : 0);
        return result;
    }
}
