package com.pkbigdata.entity;



import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by ck on 2016/6/6.
 * 用户反馈
 */
@Entity
@Table(name = "dc_user_feedback", catalog = "dc")
public class DcUserFeedback implements java.io.Serializable{

    private Integer id;//主键ID

    private Integer userId;//反馈用户id

    private String type;//反馈类型(需求，bug,活动，账号，其它)

    private String content;//反馈内容

    private String imgPath;//反馈图片路径

    private String status;//状态

    private Timestamp createDate;//创建时间

    private Timestamp updateDate;//修改时间

    private boolean delFlag;

    private  String contacts;//联系方式

    @Id
    @GeneratedValue
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "user_id",length = 11)
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Column(name = "type", length = 255)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Column(name = "content", length = 1000)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Column(name = "img_path", length = 500)
    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    @Column(name = "status", length = 255)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "create_date")
    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    @Column(name = "update_date")
    public Timestamp getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }

    @Column(name = "del_flag")
    public boolean getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(boolean delFlag) {
        this.delFlag = delFlag;
    }

    @Column(name = "contacts")
    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }
}
