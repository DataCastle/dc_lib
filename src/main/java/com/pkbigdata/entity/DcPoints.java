package com.pkbigdata.entity;

import javax.persistence.*;
import java.sql.Timestamp;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * DcPoints entity. @author MyEclipse Persistence Tools
 * 用户积分表
 */
@Entity
@Table(name = "dc_points", catalog = "dc")
public class DcPoints implements java.io.Serializable {

	// Fields

	private Integer id;
	private Float point;
	private Timestamp time;
	private String type;
	private Integer cmptId;
	private Integer userId;
	private Timestamp updateTime;

	// Constructors

	/** default constructor */
	public DcPoints() {
	}

	/** full constructor */
	public DcPoints(Float point, Timestamp time, String type, Integer cmptId,
			Integer userId) {
		this.point = point;
		this.time = time;
		this.type = type;
		this.cmptId = cmptId;
		this.userId = userId;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "point")
	public Float getPoint() {
		return this.point;
	}

	public void setPoint(Float point) {
		this.point = point;
	}

	@Column(name = "time", length = 19)
	public Timestamp getTime() {
		return this.time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	@Column(name = "type")
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "cmpt_id")
	public Integer getCmptId() {
		return this.cmptId;
	}

	public void setCmptId(Integer cmptId) {
		this.cmptId = cmptId;
	}

	@Column(name = "user_id")
	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Column(name = "update_time")
	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}
}