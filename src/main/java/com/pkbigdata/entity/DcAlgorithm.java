package com.pkbigdata.entity;// default package

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * DcAlgorithm entity. @author MyEclipse Persistence Tools
 * 算法表
 */
@Entity
@Table(name = "dc_algorithm", catalog = "dc")
public class DcAlgorithm implements java.io.Serializable {

	// Fields

	private Integer id;
	private String abbr;//算法简称
	private String fullname;//算法全称
	private String category;//算法类别
	private String jarPath;//算法jar包的位置
	private String className;//类名
	private Timestamp createTime;//创建时间

	// Constructors

	/** default constructor */
	public DcAlgorithm() {
	}

	/** full constructor */
	public DcAlgorithm(String abbr, String fullname, String category) {
		this.abbr = abbr;
		this.fullname = fullname;
		this.category = category;
	}

	// Property accessors
	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "abbr", nullable = false, length = 50)
	public String getAbbr() {
		return this.abbr;
	}

	public void setAbbr(String abbr) {
		this.abbr = abbr;
	}

	@Column(name = "fullname", nullable = false, length = 100)
	public String getFullname() {
		return this.fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	@Column(name = "category", nullable = false, length = 50)
	public String getCategory() {
		return this.category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	@Column(name = "jar_path", length = 100)
	public String getJarPath() {
		return this.jarPath;
	}

	public void setJarPath(String jarPath) {
		this.jarPath = jarPath;
	}

	@Column(name = "class_name", length = 50)
	public String getClassName() {
		return this.className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	@Column(name="create_time")
	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
}