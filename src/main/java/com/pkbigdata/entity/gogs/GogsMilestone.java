package com.pkbigdata.entity.gogs;

import javax.persistence.*;

/**
 * Created by jay on 2016/8/12.
 */
@Entity
@Table(name = "milestone", catalog = "gogs")
public class GogsMilestone implements java.io.Serializable{
    private long id;
    private Long repoId;
    private String name;
    private String content;
    private Byte isClosed;
    private Integer numIssues;
    private Integer numClosedIssues;
    private Integer completeness;
    private Long deadlineUnix;
    private Long closedDateUnix;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "repo_id")
    public Long getRepoId() {
        return repoId;
    }

    public void setRepoId(Long repoId) {
        this.repoId = repoId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "content")
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "is_closed")
    public Byte getIsClosed() {
        return isClosed;
    }

    public void setIsClosed(Byte isClosed) {
        this.isClosed = isClosed;
    }

    @Basic
    @Column(name = "num_issues")
    public Integer getNumIssues() {
        return numIssues;
    }

    public void setNumIssues(Integer numIssues) {
        this.numIssues = numIssues;
    }

    @Basic
    @Column(name = "num_closed_issues")
    public Integer getNumClosedIssues() {
        return numClosedIssues;
    }

    public void setNumClosedIssues(Integer numClosedIssues) {
        this.numClosedIssues = numClosedIssues;
    }

    @Basic
    @Column(name = "completeness")
    public Integer getCompleteness() {
        return completeness;
    }

    public void setCompleteness(Integer completeness) {
        this.completeness = completeness;
    }

    @Basic
    @Column(name = "deadline_unix")
    public Long getDeadlineUnix() {
        return deadlineUnix;
    }

    public void setDeadlineUnix(Long deadlineUnix) {
        this.deadlineUnix = deadlineUnix;
    }

    @Basic
    @Column(name = "closed_date_unix")
    public Long getClosedDateUnix() {
        return closedDateUnix;
    }

    public void setClosedDateUnix(Long closedDateUnix) {
        this.closedDateUnix = closedDateUnix;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GogsMilestone that = (GogsMilestone) o;

        if (id != that.id) return false;
        if (repoId != null ? !repoId.equals(that.repoId) : that.repoId != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (content != null ? !content.equals(that.content) : that.content != null) return false;
        if (isClosed != null ? !isClosed.equals(that.isClosed) : that.isClosed != null) return false;
        if (numIssues != null ? !numIssues.equals(that.numIssues) : that.numIssues != null) return false;
        if (numClosedIssues != null ? !numClosedIssues.equals(that.numClosedIssues) : that.numClosedIssues != null)
            return false;
        if (completeness != null ? !completeness.equals(that.completeness) : that.completeness != null) return false;
        if (deadlineUnix != null ? !deadlineUnix.equals(that.deadlineUnix) : that.deadlineUnix != null) return false;
        if (closedDateUnix != null ? !closedDateUnix.equals(that.closedDateUnix) : that.closedDateUnix != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (repoId != null ? repoId.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (isClosed != null ? isClosed.hashCode() : 0);
        result = 31 * result + (numIssues != null ? numIssues.hashCode() : 0);
        result = 31 * result + (numClosedIssues != null ? numClosedIssues.hashCode() : 0);
        result = 31 * result + (completeness != null ? completeness.hashCode() : 0);
        result = 31 * result + (deadlineUnix != null ? deadlineUnix.hashCode() : 0);
        result = 31 * result + (closedDateUnix != null ? closedDateUnix.hashCode() : 0);
        return result;
    }
}
