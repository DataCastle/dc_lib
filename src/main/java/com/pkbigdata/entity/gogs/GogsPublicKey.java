package com.pkbigdata.entity.gogs;

import javax.persistence.*;

/**
 * Created by jay on 2016/8/12.
 */
@Entity
@Table(name = "public_key", catalog = "gogs")
public class GogsPublicKey implements java.io.Serializable{
    private long id;
    private long ownerId;
    private String name;
    private String fingerprint;
    private String content;
    private int mode;
    private int type;
    private Long createdUnix;
    private Long updatedUnix;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "owner_id")
    public long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "fingerprint")
    public String getFingerprint() {
        return fingerprint;
    }

    public void setFingerprint(String fingerprint) {
        this.fingerprint = fingerprint;
    }

    @Basic
    @Column(name = "content")
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "mode")
    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    @Basic
    @Column(name = "type")
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Basic
    @Column(name = "created_unix")
    public Long getCreatedUnix() {
        return createdUnix;
    }

    public void setCreatedUnix(Long createdUnix) {
        this.createdUnix = createdUnix;
    }

    @Basic
    @Column(name = "updated_unix")
    public Long getUpdatedUnix() {
        return updatedUnix;
    }

    public void setUpdatedUnix(Long updatedUnix) {
        this.updatedUnix = updatedUnix;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GogsPublicKey that = (GogsPublicKey) o;

        if (id != that.id) return false;
        if (ownerId != that.ownerId) return false;
        if (mode != that.mode) return false;
        if (type != that.type) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (fingerprint != null ? !fingerprint.equals(that.fingerprint) : that.fingerprint != null) return false;
        if (content != null ? !content.equals(that.content) : that.content != null) return false;
        if (createdUnix != null ? !createdUnix.equals(that.createdUnix) : that.createdUnix != null) return false;
        if (updatedUnix != null ? !updatedUnix.equals(that.updatedUnix) : that.updatedUnix != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (ownerId ^ (ownerId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (fingerprint != null ? fingerprint.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + mode;
        result = 31 * result + type;
        result = 31 * result + (createdUnix != null ? createdUnix.hashCode() : 0);
        result = 31 * result + (updatedUnix != null ? updatedUnix.hashCode() : 0);
        return result;
    }
}
