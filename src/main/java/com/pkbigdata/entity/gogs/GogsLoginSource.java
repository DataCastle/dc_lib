package com.pkbigdata.entity.gogs;

import javax.persistence.*;

/**
 * Created by jay on 2016/8/12.
 */
@Entity
@Table(name = "login_source", catalog = "gogs")
public class GogsLoginSource implements java.io.Serializable{
    private long id;
    private Integer type;
    private String name;
    private byte isActived;
    private String cfg;
    private Long createdUnix;
    private Long updatedUnix;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "type")
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "is_actived")
    public byte getIsActived() {
        return isActived;
    }

    public void setIsActived(byte isActived) {
        this.isActived = isActived;
    }

    @Basic
    @Column(name = "cfg")
    public String getCfg() {
        return cfg;
    }

    public void setCfg(String cfg) {
        this.cfg = cfg;
    }

    @Basic
    @Column(name = "created_unix")
    public Long getCreatedUnix() {
        return createdUnix;
    }

    public void setCreatedUnix(Long createdUnix) {
        this.createdUnix = createdUnix;
    }

    @Basic
    @Column(name = "updated_unix")
    public Long getUpdatedUnix() {
        return updatedUnix;
    }

    public void setUpdatedUnix(Long updatedUnix) {
        this.updatedUnix = updatedUnix;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GogsLoginSource that = (GogsLoginSource) o;

        if (id != that.id) return false;
        if (isActived != that.isActived) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (cfg != null ? !cfg.equals(that.cfg) : that.cfg != null) return false;
        if (createdUnix != null ? !createdUnix.equals(that.createdUnix) : that.createdUnix != null) return false;
        if (updatedUnix != null ? !updatedUnix.equals(that.updatedUnix) : that.updatedUnix != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (int) isActived;
        result = 31 * result + (cfg != null ? cfg.hashCode() : 0);
        result = 31 * result + (createdUnix != null ? createdUnix.hashCode() : 0);
        result = 31 * result + (updatedUnix != null ? updatedUnix.hashCode() : 0);
        return result;
    }
}
