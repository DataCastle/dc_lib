package com.pkbigdata.entity.bbs;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * BbsNotification entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "bbs_notification", catalog = "bbs")

public class BbsNotification implements java.io.Serializable {

	// Fields

	private Integer id;
	private String message;//消息内容
	private Integer read;//是否已读
	private Integer fromAuthorId;//来源用户id
	private Integer authorId;//目标用户id
	private Integer tid;//帖子id
	private Integer rid;//回复id
	private Timestamp inTime;

	// Constructors

	/** default constructor */
	public BbsNotification() {
	}

	/** minimal constructor */
	public BbsNotification(String message, Integer read, Integer fromAuthorId, Integer authorId, Timestamp inTime) {
		this.message = message;
		this.read = read;
		this.fromAuthorId = fromAuthorId;
		this.authorId = authorId;
		this.inTime = inTime;
	}

	/** full constructor */
	public BbsNotification(String message, Integer read, Integer fromAuthorId, Integer authorId, Integer tid,
			Integer rid, Timestamp inTime) {
		this.message = message;
		this.read = read;
		this.fromAuthorId = fromAuthorId;
		this.authorId = authorId;
		this.tid = tid;
		this.rid = rid;
		this.inTime = inTime;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "message", nullable = false)

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Column(name = "read", nullable = false)

	public Integer getRead() {
		return this.read;
	}

	public void setRead(Integer read) {
		this.read = read;
	}

	@Column(name = "from_author_id", nullable = false)

	public Integer getFromAuthorId() {
		return this.fromAuthorId;
	}

	public void setFromAuthorId(Integer fromAuthorId) {
		this.fromAuthorId = fromAuthorId;
	}

	@Column(name = "author_id", nullable = false)

	public Integer getAuthorId() {
		return this.authorId;
	}

	public void setAuthorId(Integer authorId) {
		this.authorId = authorId;
	}

	@Column(name = "tid")

	public Integer getTid() {
		return this.tid;
	}

	public void setTid(Integer tid) {
		this.tid = tid;
	}

	@Column(name = "rid")

	public Integer getRid() {
		return this.rid;
	}

	public void setRid(Integer rid) {
		this.rid = rid;
	}

	@Column(name = "in_time", nullable = false, length = 19)

	public Timestamp getInTime() {
		return this.inTime;
	}

	public void setInTime(Timestamp inTime) {
		this.inTime = inTime;
	}

}