package com.pkbigdata.entity.bbs;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * BbsBanner entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "bbs_banner", catalog = "bbs")

public class BbsBanner implements java.io.Serializable {

	// Fields

	private Integer id;
	private String img;//图片路径
	private String url;//图片跳转路径
	private Integer order;//排序
	private Boolean status;//状态，如果是true则显示，false不显示

	// Constructors

	/** default constructor */
	public BbsBanner() {
	}

	/** full constructor */
	public BbsBanner(String img, String url, Integer order, Boolean status) {
		this.img = img;
		this.url = url;
		this.order = order;
		this.status = status;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "img", length = 500)

	public String getImg() {
		return this.img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	@Column(name = "url", length = 500)

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Column(name = "order")

	public Integer getOrder() {
		return this.order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	@Column(name = "status")

	public Boolean getStatus() {
		return this.status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

}