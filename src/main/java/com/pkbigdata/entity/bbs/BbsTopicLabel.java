package com.pkbigdata.entity.bbs;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * BbsTopicLabel entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "bbs_topic_label", catalog = "bbs")

public class BbsTopicLabel implements java.io.Serializable {

	// Fields

	private Integer id;
	private Integer tid;//贴子id
	private Integer lid;//标签id

	// Constructors

	/** default constructor */
	public BbsTopicLabel() {
	}

	/** full constructor */
	public BbsTopicLabel(Integer tid, Integer lid) {
		this.tid = tid;
		this.lid = lid;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "tid")

	public Integer getTid() {
		return this.tid;
	}

	public void setTid(Integer tid) {
		this.tid = tid;
	}

	@Column(name = "lid")

	public Integer getLid() {
		return this.lid;
	}

	public void setLid(Integer lid) {
		this.lid = lid;
	}

}