package com.pkbigdata.entity.bbs;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * BbsOperationLog entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "bbs_operation_log", catalog = "bbs")

public class BbsOperationLog implements java.io.Serializable {

	// Fields

	private Integer id;
	private String ip;//访问者id
	private Integer userId;//用户id
	private String username;//用户名称
	private String url;//访问路径
	private String type;//操作类型
	private String content;//操作内容
	private Timestamp createTime;
	private String source;//渠道
	private String userAgent;
	private String browser;//浏览器
	private String sessionId;
	private String remarks;
	private String portType;

	// Constructors

	/** default constructor */
	public BbsOperationLog() {
	}

	/** full constructor */
	public BbsOperationLog(String ip, Integer userId, String username, String url, String type, String content,
			Timestamp createTime, String remarks, String source, String userAgent, String browser, String sessionId,
			String portType) {
		this.ip = ip;
		this.userId = userId;
		this.username = username;
		this.url = url;
		this.type = type;
		this.content = content;
		this.createTime = createTime;
		this.remarks = remarks;
		this.source = source;
		this.userAgent = userAgent;
		this.browser = browser;
		this.sessionId = sessionId;
		this.portType = portType;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "ip")

	public String getIp() {
		return this.ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	@Column(name = "user_id")

	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Column(name = "username")

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Column(name = "url", length = 10000)

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Column(name = "type")

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "content")

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Column(name = "create_time", length = 19)

	public Timestamp getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	@Column(name = "remarks")

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Column(name = "source")

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	@Column(name = "user_agent", length = 1000)

	public String getUserAgent() {
		return this.userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	@Column(name = "browser")

	public String getBrowser() {
		return this.browser;
	}

	public void setBrowser(String browser) {
		this.browser = browser;
	}

	@Column(name = "session_id")

	public String getSessionId() {
		return this.sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	@Column(name = "port_type")

	public String getPortType() {
		return this.portType;
	}

	public void setPortType(String portType) {
		this.portType = portType;
	}

}