package com.pkbigdata.entity.bbs;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * BbsNews entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "bbs_news", catalog = "bbs")

public class BbsNews implements java.io.Serializable {

	// Fields

	private Integer id;
	private String title;//名称
	private String content;//内容
	private String imgPath;//图片路径
	private Boolean delFlag;//删除标记
	private Boolean showStatus;//显示状态
	private Integer createBy;//创建人
	private Timestamp createTime;//创建时间
	private Integer updateBy;//修改人
	private Timestamp updateTime;//修改时间

	// Constructors

	/** default constructor */
	public BbsNews() {
	}

	/** minimal constructor */
	public BbsNews(String title, String content, String imgPath, Boolean delFlag, Boolean showStatus, Integer createBy,
			Timestamp createTime) {
		this.title = title;
		this.content = content;
		this.imgPath = imgPath;
		this.delFlag = delFlag;
		this.showStatus = showStatus;
		this.createBy = createBy;
		this.createTime = createTime;
	}

	/** full constructor */
	public BbsNews(String title, String content, String imgPath, Boolean delFlag, Boolean showStatus, Integer createBy,
			Timestamp createTime, Integer updateBy, Timestamp updateTime) {
		this.title = title;
		this.content = content;
		this.imgPath = imgPath;
		this.delFlag = delFlag;
		this.showStatus = showStatus;
		this.createBy = createBy;
		this.createTime = createTime;
		this.updateBy = updateBy;
		this.updateTime = updateTime;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "title", nullable = false)

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "content", nullable = false)

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Column(name = "img_path", nullable = false)

	public String getImgPath() {
		return this.imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	@Column(name = "del_flag", nullable = false)

	public Boolean getDelFlag() {
		return this.delFlag;
	}

	public void setDelFlag(Boolean delFlag) {
		this.delFlag = delFlag;
	}

	@Column(name = "show_status", nullable = false)

	public Boolean getShowStatus() {
		return this.showStatus;
	}

	public void setShowStatus(Boolean showStatus) {
		this.showStatus = showStatus;
	}

	@Column(name = "create_by", nullable = false)

	public Integer getCreateBy() {
		return this.createBy;
	}

	public void setCreateBy(Integer createBy) {
		this.createBy = createBy;
	}

	@Column(name = "create_time", nullable = false, length = 19)

	public Timestamp getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	@Column(name = "update_by")

	public Integer getUpdateBy() {
		return this.updateBy;
	}

	public void setUpdateBy(Integer updateBy) {
		this.updateBy = updateBy;
	}

	@Column(name = "update_time", length = 19)

	public Timestamp getUpdateTime() {
		return this.updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

}