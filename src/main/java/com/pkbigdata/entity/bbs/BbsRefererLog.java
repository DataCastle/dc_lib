package com.pkbigdata.entity.bbs;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * BbsRefererLog entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "bbs_referer_log", catalog = "bbs")

public class BbsRefererLog implements java.io.Serializable {

	// Fields

	private Integer id;
	private String ip;
	private String url;
	private Timestamp createTime;
	private String remarks;

	// Constructors

	/** default constructor */
	public BbsRefererLog() {
	}

	/** full constructor */
	public BbsRefererLog(String ip, String url, Timestamp createTime, String remarks) {
		this.ip = ip;
		this.url = url;
		this.createTime = createTime;
		this.remarks = remarks;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "ip")

	public String getIp() {
		return this.ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	@Column(name = "url", length = 1000)

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Column(name = "create_time", length = 19)

	public Timestamp getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	@Column(name = "remarks")

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}