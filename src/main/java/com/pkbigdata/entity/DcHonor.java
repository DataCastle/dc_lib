package com.pkbigdata.entity;// default package

import javax.persistence.*;
import java.sql.Timestamp;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * DcHonor entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "dc_honor", catalog = "dc")
public class DcHonor implements java.io.Serializable {

	// Fields

	private Integer id;
	private String name;
	private Timestamp time;
	private Integer userId;

	// Constructors

	/** default constructor */
	public DcHonor() {
	}

	/** full constructor */
	public DcHonor(String name, Timestamp time, Integer userId) {
		this.name = name;
		this.time = time;
		this.userId = userId;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "name", length = 500)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "time", length = 19)
	public Timestamp getTime() {
		return this.time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	@Column(name = "user_id")
	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

}