package com.pkbigdata.entity.util;

import javax.persistence.*;

/**
 * Created by ck on 2017/2/9.
 */
@Entity
@Table(name = "dc_short_url",catalog = "dc_util")
public class DcShortUrl {

    private Integer id;
    private String shortUrl;
    private String longUrl;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "short_url",nullable = false)
    public String getShortUrl() {
        return shortUrl;
    }

    public void setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
    }

    @Column(name = "long_url",nullable = false)
    public String getLongUrl() {
        return longUrl;
    }

    public void setLongUrl(String longUrl) {
        this.longUrl = longUrl;
    }
}
