package com.pkbigdata.util;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

/**
 * Created by zhaorenjie on 2015/4/27.
 */
public class JsonToMap {

    /**
     * json字符串转map
     * @param json json
     * @return
     */
    public static Map jsonToMap(String json){
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Object> maps = null;
        try {
            maps = objectMapper.readValue(json, Map.class);
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return maps;
    }

    /**
     * json字符串转List<map></map>
     * @param json json
     * @return
     */
    public static Map[] jsonToListMap(String json){
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Object>[] maps = null;
        try {
            maps = objectMapper.readValue(json, Map[].class);
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return maps;
    }

    public static String beanToJson(Object o){
        ObjectMapper objectMapper = new ObjectMapper();
        StringWriter writer = new StringWriter();
        try {
            objectMapper.writeValue(writer,o);

            return writer.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "";
    }


}
