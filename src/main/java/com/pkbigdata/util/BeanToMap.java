package com.pkbigdata.util;


import com.pkbigdata.entity.DcTeam;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zhaorenjie on 2015/4/17.
 * 对list创建索引
 */
public class BeanToMap {

    /**
     * 将list<map>转为map,用其中一个key作为索引
     * @param key
     * @param list
     * @return
     */
    public static Map<String,Map> listToMap(String key ,List<Map> list){
        Map<String,Map> result= new HashMap<String,Map>();
        for (int i = 0; i <list.size() ; i++) {
            result.put(list.get(i).get(key)+"",list.get(i));
        }
        return result;
    }

    /**
     * 用T的某一个属性作为key生成索引
     *
     * @param filed
     * @param list
     * @param <T>
     * @return
     */
    public static <T> Map<Integer, T> toMap(String filed, List<T> list) {

        Map<Integer, T> map = new HashMap<Integer, T>();
        String getMethodName = "get"
                + filed.substring(0, 1).toUpperCase()
                + filed.substring(1);
        Class tCls = null;
        if (list.size() > 0) {
            tCls = list.get(0).getClass();
        } else {
            return map;
        }
        for (T t : list) {
            try {
                Method getMethod = tCls.getMethod(getMethodName,
                        new Class[]{});
                Integer value = (Integer) getMethod.invoke(t, new Object[]{});
                map.put(value, t);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        return map;

    }

    /**
     * 将一个 JavaBean 对象转化为一个  Map
     * @param bean 要转化的JavaBean 对象
     * @return 转化出来的  Map 对象
     * @throws IntrospectionException 如果分析类属性失败
     * @throws IllegalAccessException 如果实例化 JavaBean 失败
     * @throws InvocationTargetException 如果调用属性的 setter 方法失败
     */
    public static Map convertBean(Object bean)
            throws IntrospectionException, IllegalAccessException, InvocationTargetException {
        Class type = bean.getClass();
        Map returnMap = new HashMap();
        BeanInfo beanInfo = Introspector.getBeanInfo(type);

        PropertyDescriptor[] propertyDescriptors =  beanInfo.getPropertyDescriptors();
        for (int i = 0; i< propertyDescriptors.length; i++) {
            PropertyDescriptor descriptor = propertyDescriptors[i];
            String propertyName = descriptor.getName();
            if (!propertyName.equals("class")) {
                Method readMethod = descriptor.getReadMethod();
                Object result = readMethod.invoke(bean, new Object[0]);
                if (result != null) {
                    returnMap.put(propertyName, result);
                } else {
                    returnMap.put(propertyName, "");
                }
            }
        }
        return returnMap;
    }

    /**
     * 将实体列表转化为map列表
     * @param beans 实体列表
     * @return
     * @throws IllegalAccessException
     * @throws IntrospectionException
     * @throws InvocationTargetException
     */
    public static List<Map> convertBeans(List<Object> beans) throws IllegalAccessException, IntrospectionException, InvocationTargetException {
        List<Map> maps = new ArrayList<Map>();
        if (beans==null){
            return maps;
        }
        for (int i = 0; i < beans.size(); i++) {
            Map map = convertBean(beans.get(i));
            maps.add(map);
        }
        return maps;
    }
    /**
     * 将实体列表转化为map列表
     * @param beans 实体列表
     * @return
     * @throws IllegalAccessException
     * @throws IntrospectionException
     * @throws InvocationTargetException
     */
    public static List<Map> convertBeansByTeam(List<DcTeam> beans) throws IllegalAccessException, IntrospectionException, InvocationTargetException {
        List<Map> maps = new ArrayList<Map>();
        if (beans==null){
            return maps;
        }
        for (int i = 0; i < beans.size(); i++) {
            Map map = convertBean(beans.get(i));
            maps.add(map);
        }
        return maps;
    }


}
