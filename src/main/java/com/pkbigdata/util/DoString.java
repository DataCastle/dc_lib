package com.pkbigdata.util;

/**
 * 封装了处理字符串的静态方法
 *
 * @author Administrator
 */
public class DoString {
    /**
     * 处理uid中出现的逗号和下划线
     * 将逗号替换为双中线--，下划线替换为中线-，均为英文。
     *
     * @param uid
     * @return
     */
    public static String doString(String uid) {
        if (uid == null) {
            return null;
        }
        char[] mind = uid.toCharArray();
        String newUid = "";
        for (char c : mind) {
            if (",".equals(String.valueOf(c))) {
                newUid += "-----";
            } else {
                newUid += String.valueOf(c);
            }
        }
        return newUid;
    }

    /**
     * 处理uid中的双--,和单-
     * 将双中线替换为逗号,将单中线替换为下划线_
     *
     * @param uid
     * @return
     */
    public static String DoString2(String uid) {
        String newUid;
        newUid = uid.replaceAll("-----", ",");
        return newUid;
    }

}
