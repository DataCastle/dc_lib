package com.pkbigdata.annotation

import java.lang.annotation.*

/**
 * Created by ck on 2016/12/5.
 */

@Target([ElementType.METHOD, ElementType.TYPE ])
@Retention(RetentionPolicy.RUNTIME)
@Documented
@interface UserMessage {
    String content() default "";
}